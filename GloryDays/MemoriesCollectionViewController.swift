//
//  MemoriesCollectionViewController.swift
//  GloryDays
//
//  Created by Dev on 12/6/17.
//  Copyright © 2017 kedavra. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import Speech

import CoreSpotlight
import MobileCoreServices


private let reuseIdentifier = "cell"

class MemoriesCollectionViewController: UICollectionViewController, UINavigationControllerDelegate,
			UIImagePickerControllerDelegate,AVAudioRecorderDelegate{

	var memories: [URL] = []
	
	var audioPlayer: AVAudioPlayer?
	var audioRecorder: AVAudioRecorder?
	var recordingURL: URL!
	
	override func viewDidLoad() {
        super.viewDidLoad()
		
		
		navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.addImagePressed))
		
		self.recordingURL = getDocumentDirectory().appendingPathComponent("memory-recording.m4a")
		
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
		// este codigo lo genera automaticamente pero no sirve si creamos una celda personalizada
        // self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
    }

	
	override func viewDidAppear(_ animated: Bool) {
		// buena practica hacer transiciones a otros views aqui en lugar del didload dado q en didLoad la vista actual aun no se cargó
		super.viewDidAppear(animated)
		
		checkForGrantedPermissions()
		
		self.loadMemories()
		
	}
	
	func checkForGrantedPermissions(){
		let photosAuth: Bool = PHPhotoLibrary.authorizationStatus() == .authorized
		let recordingAuth: Bool = AVAudioSession.sharedInstance().recordPermission() == .granted
		let transcriptionAuth: Bool = SFSpeechRecognizer.authorizationStatus() == .authorized
		
		let authorized = photosAuth && recordingAuth && transcriptionAuth
		if !authorized {
			if let vc = storyboard?.instantiateViewController(withIdentifier: "ShowTerms") {
				navigationController?.present(vc, animated: true)
			}
		}
		
	}
	
	func loadMemories(){
		memories.removeAll()
		let directory:URL = getDocumentDirectory()
		
		guard let files = try? FileManager.default.contentsOfDirectory(at: directory, includingPropertiesForKeys: nil, options: [])else{
			
			return
			
		}
		
		
		for file in files {
			if file.lastPathComponent == "" { continue }
			if file.lastPathComponent.hasSuffix(".thumb"){
				let noExtension = file.lastPathComponent.replacingOccurrences(of: ".thumb", with: "")
				memories.append(directory.appendingPathComponent(noExtension))
			}
		}
		
		
		collectionView?.reloadSections( IndexSet(integer: 1)  ) //arranca en 1 pq en 0 esta el buscador
	}
	
	func getDocumentDirectory()->URL{
		let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
		
		return paths[0]
	}
	
	
	func addImagePressed(){
			let vc = UIImagePickerController()
		vc.modalPresentationStyle = .popover
		vc.delegate = self
		navigationController?.present(vc, animated: true)
	}
	
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
		if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
			self.addNewMemory(image: image)
			self.loadMemories()
			
			dismiss(animated: true)
		}
	}
	
	func addNewMemory(image:UIImage){
		let memoryName = "memory-\(Date().timeIntervalSince1970)"
		let imageName = "\(memoryName).jpg"
		let thumbName =  "\(memoryName).thumb"
		
		do{
			
			let imagePath = getDocumentDirectory().appendingPathComponent(imageName)
			
			if let jpegData = UIImageJPEGRepresentation(image, 80) {
				try jpegData.write(to: imagePath, options: [.atomicWrite])
			}
			
			if let thumbnail = resizeImage(image: image, to: 200) {
				let thumbPath = getDocumentDirectory().appendingPathComponent(thumbName)
				if let thumbData = UIImageJPEGRepresentation(thumbnail, 80) {
					try thumbData.write(to: thumbPath, options: [.atomicWrite])
				}
			}
			
			
			
		}catch{
			print("Ha ocurrido un error")
		}
		
		
	}
	
	func resizeImage(image:UIImage, to width:CGFloat)->UIImage?{
		let scale = width / image.size.width
		let height = image.size.height * scale
		
		// creo un "canvas" un area de dibujo
		UIGraphicsBeginImageContextWithOptions(CGSize(width: width, height:height), false, 0)
		
		//dibujo la imagen
		image.draw(in: CGRect(x: 0, y: 0, width: width, height: height))
		
		//creo la imagen nueva con las dimensiones nuveas
		let newImage = UIGraphicsGetImageFromCurrentImageContext()
		
		//finalizo la edicion
		UIGraphicsEndImageContext()
		
		return newImage
	}
	
	func getImageURL(from memory:URL)-> URL{
		return memory.appendingPathExtension("jpg")
	}
	func getThumbnailURL(from memory:URL)-> URL{
		return memory.appendingPathExtension("thumb")
	}
	func getAudioURL(from memory:URL)-> URL{
		return memory.appendingPathExtension("m4a")
	}
	func getTranscriptionURL(from memory:URL)-> URL{
		return memory.appendingPathExtension("txt")
	}
	
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
		if section == 0 {
			return 0
		}
		
        return memories.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! MemoryCell
    
        // Configure the cell
		
		let memoryName = getImageURL(from: self.memories[indexPath.row]).path 
		if let image = UIImage(contentsOfFile: memoryName) {
			cell.imageView.image = image
		}
		
		if cell.gestureRecognizers == nil {
			
			let recognizer = UILongPressGestureRecognizer(target: self, action: #selector(memoryLongPressed))
			recognizer.minimumPressDuration = 0.3
			
			cell.layer.borderColor = UIColor.white.cgColor
			cell.layer.borderWidth = 4 //pixels
			cell.layer.cornerRadius = 10
			
		}
		
        return cell
    }
	
	var currentMemory: URL!
	
	func memoryLongPressed(sender :UILongPressGestureRecognizer){
		if sender.state == .began {
			let cell = sender.view as! MemoryCell
			if let index = collectionView?.indexPath(for: cell) {
				currentMemory = memories[index.row]
				startRecordingMemory()
			}
		}
		
		if sender.state == .ended {
			finishRecordingMemory(success: true)
			
		}
	}
	
	func startRecordingMemory(){
		
		audioPlayer?.stop() // detiene el audio player en el caso que estuviese reproduciendo algo
		
		collectionView?.backgroundColor = UIColor(red: 0.6, green: 0.0, blue: 0.0, alpha: 1.0)
		
		let recordingSession = AVAudioSession.sharedInstance()
		do{
			
			try recordingSession.setCategory(AVAudioSessionCategoryPlayAndRecord, with: .defaultToSpeaker)
			try recordingSession.setActive(true)
			
			let s = [ AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
			                 AVSampleRateKey: 44100,
			                 AVNumberOfChannelsKey: 2,
			                 AVEncoderAudioQualityKey: AVAudioQuality.high] as [String : Any]
			
			audioRecorder = try AVAudioRecorder(url: recordingURL, settings: s)
			audioRecorder?.delegate = self
			audioRecorder?.record()
			
			
			
		}catch let error{
			print(error)
			finishRecordingMemory(success: false)
		}
		
	}
	
	
	func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
		if !flag {
			finishRecordingMemory(success: false)
		}
	}
	
	
	func finishRecordingMemory(success: Bool){
		collectionView?.backgroundColor = UIColor(red: 97.0, green: 86.0, blue: 110.0, alpha: 1.0)
		
		audioRecorder?.stop()
		if success {
			do {
				let audioMemoryURL = self.currentMemory.appendingPathExtension("m4a")
				
				let fileManager = FileManager.default
				
				if fileManager.fileExists(atPath: audioMemoryURL.path) {
					try fileManager.removeItem(at: audioMemoryURL)
				}
				
				try fileManager.moveItem(at: recordingURL, to: audioMemoryURL)
				
				self.transcribeAudioToText(memory: currentMemory)
				
			} catch let error {
				print("Ha habido un error \(error)")
			}
		}
	}
	
	func transcribeAudioToText(memory: URL){
		
		let audio = getAudioURL(from: memory)
		let transcription = getTranscriptionURL(from: memory)
		
		let recognizer = SFSpeechRecognizer()
		let request = SFSpeechURLRecognitionRequest(url: audio)
		
		recognizer?.recognitionTask(with: request, resultHandler: {[unowned self] (result, error) in
			
			guard let result = result else {
				
				print("Ha habido un error \(error)")
				return
				
			}
			
			if result.isFinal {
				
				let texto = result.bestTranscription.formattedString
				
				do {
					
					try texto.write(to: transcription, atomically: true, encoding: String.Encoding.utf8)
					
				}catch let error{
					print("Ha habido un error al guardar la transcripcion \(error)")
				}
				
			}
			
			
		})
		
	}
	
	

	override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
		let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath)
		
		return header
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int)->CGSize{
	
		// poner los headers q no queremos con altura 0 para que no aparezcan
		
		if section == 0 {
			return CGSize(width: 0, height: 50)
		}else{
			return CGSize.zero
		}
	}
	
	
	override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		let memory = memories[indexPath.row]
		
		let fileManager = FileManager.default
		
		
		do {
		
			let audioName = getAudioURL(from: memory)
			let transcriptionName = getTranscriptionURL(from: memory)
			
			if fileManager.fileExists(atPath: audioName.path) {
				audioPlayer = try AVAudioPlayer(contentsOf: audioName)
				audioPlayer?.play()
		
			}
		
			if fileManager.fileExists(atPath: transcriptionName.path) {
				let contents = try String(contentsOf: transcriptionName)
				print("Transcripcion: \(contents)")
				
			}
		
			
		}catch{
			print("Ha habido un error ")
		}
		
		
	}
	
	
    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
