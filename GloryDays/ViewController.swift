//
//  ViewController.swift
//  GloryDays
//
//  Created by Dev on 11/6/17.
//  Copyright © 2017 kedavra. All rights reserved.
//

import UIKit

import AVFoundation
import Photos
import Speech


class ViewController: UIViewController {

	@IBOutlet weak var infoLabel: UILabel!
	@IBAction func askForPermissions(_ sender: UIButton) {
		self.askForPhotosPermission()
	}
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	
	func askForPhotosPermission(){
		PHPhotoLibrary.requestAuthorization { [unowned self] (status) in
			
			DispatchQueue.main.async {
				//indica al proceso que el bloque debe procesarse en el hilo principal
				if status == .authorized {
					self.askForRecordPermission()
				}else{
					self.infoLabel.text = "Nos has denegado el permiso para acceder a fotos. Por favor, actívalo en los ajustes de tu dispositivo para continuar."
				}
			}
		}
	}
	
	func askForRecordPermission(){
		
		AVAudioSession.sharedInstance().requestRecordPermission { (allowed) in
			DispatchQueue.main.async {
				if allowed {
						self.askforTranscriptionPermission()
				}else{
					self.infoLabel.text = "Nos has denegado el permiso de grabacion de audio. Por favor, actívalo en los ajustes de tu dispositivo para continuar."
				}
			}
		}
		
	}
	
	func askforTranscriptionPermission(){
		
		SFSpeechRecognizer.requestAuthorization { (status) in
			
			DispatchQueue.main.async {
				
				if status == .authorized {
					self.autorizationCompleted()
				}else{
					self.infoLabel.text = "Nos has denegado el permiso para transcripcion de texto. Por favor, actívalo en los ajustes de tu dispositivo para continuar."
				}
			}
			
		}
		
	}
	
	func autorizationCompleted(){
		
		self.dismiss(animated: true)
		
	}
	
	
	

}

